<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@getHome');
Route::get('/pt', 'PagesController@getTcle');
Route::get('/pt/pt_dados_participantes', 'PagesController@getPersonalData');
Route::get('/pt/pt_descricao', 'PagesController@getDescription');
Route::get('/pt/pt_viz1', 'PagesController@getViz1');
Route::get('/pt/pt_likert_viz1', 'PagesController@getLikertViz1');
Route::get('/pt/pt_viz2', 'PagesController@getViz2');
Route::get('/pt/pt_likert_viz2', 'PagesController@getLikertViz2');
Route::get('/pt/pt_viz3', 'PagesController@getViz3');
Route::get('/pt/pt_likert_viz3', 'PagesController@getLikertViz3');
Route::get('/pt/pt_viz4', 'PagesController@getViz4');
Route::get('/pt/pt_likert_viz4', 'PagesController@getLikertViz4');
Route::get('/pt/pt_agradecimentos', 'PagesController@getThanksPage');

Route::post('/storeTcle', 'SurveysController@storeTcle');
Route::post('/storePersonalData', 'SurveysController@storePersonalData');
Route::post('/storeDescription', 'SurveysController@storeDescription');
Route::post('/storeViz1', 'SurveysController@storeViz1');
Route::post('/storeLikertViz1', 'SurveysController@storeLikertViz1');
Route::post('/storeViz2', 'SurveysController@storeViz2');
Route::post('/storeLikertViz2', 'SurveysController@storeLikertViz2');
Route::post('/storeViz3', 'SurveysController@storeViz3');
Route::post('/storeLikertViz3', 'SurveysController@storeLikertViz3');
Route::post('/storeViz4', 'SurveysController@storeViz4');
Route::post('/storeLikertViz4', 'SurveysController@storeLikertViz4');