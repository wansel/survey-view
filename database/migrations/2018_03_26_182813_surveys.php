<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Surveys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('accept');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('gender')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('education_level')->nullable();
            $table->string('occupation')->nullable();
            $table->string('education_institution')->nullable();
            $table->string('occupation_work_regime')->nullable();
            $table->string('occupation_time')->nullable();
            $table->string('occupation_level')->nullable();
            $table->string('technology_use')->nullable();
            $table->string('time_in_user_data')->nullable();
            $table->string('description')->nullable();
            $table->string('time_in_description')->nullable();
            $table->string('viz1_1')->nullable();
            $table->string('viz1_2')->nullable();
            $table->string('viz1_3')->nullable();
            $table->string('viz1_4')->nullable();
            $table->string('viz1_5')->nullable();
            $table->string('viz1_6')->nullable();
            $table->string('viz1_7')->nullable();
            $table->string('viz1_8')->nullable();
            $table->string('time_in_viz1')->nullable();
            $table->integer('likert_viz1_1')->nullable();
            $table->integer('likert_viz1_2')->nullable();
            $table->integer('likert_viz1_3')->nullable();
            $table->integer('likert_viz1_4')->nullable();
            $table->integer('likert_viz1_5')->nullable();
            $table->integer('likert_viz1_6')->nullable();
            $table->integer('likert_viz1_7')->nullable();
            $table->integer('likert_viz1_8')->nullable();
            $table->integer('likert_viz1_9')->nullable();
            $table->integer('likert_viz1_10')->nullable();
            $table->integer('likert_viz1_11')->nullable();
            $table->integer('likert_viz1_12')->nullable();
            $table->string('time_in_likert_viz1')->nullable();
            $table->string('viz2_1')->nullable();
            $table->string('viz2_2')->nullable();
            $table->string('viz2_3')->nullable();
            $table->string('viz2_4')->nullable();
            $table->string('viz2_5')->nullable();
            $table->string('viz2_6')->nullable();
            $table->string('viz2_7')->nullable();
            $table->string('viz2_8')->nullable();
            $table->string('time_in_viz2')->nullable();
            $table->integer('likert_viz2_1')->nullable();
            $table->integer('likert_viz2_2')->nullable();
            $table->integer('likert_viz2_3')->nullable();
            $table->integer('likert_viz2_4')->nullable();
            $table->integer('likert_viz2_5')->nullable();
            $table->integer('likert_viz2_6')->nullable();
            $table->integer('likert_viz2_7')->nullable();
            $table->integer('likert_viz2_8')->nullable();
            $table->integer('likert_viz2_9')->nullable();
            $table->integer('likert_viz2_10')->nullable();
            $table->integer('likert_viz2_11')->nullable();
            $table->integer('likert_viz2_12')->nullable();
            $table->string('time_in_likert_viz2')->nullable();
            $table->string('viz3_1')->nullable();
            $table->string('viz3_2')->nullable();
            $table->string('viz3_3')->nullable();
            $table->string('viz3_4')->nullable();
            $table->string('viz3_5')->nullable();
            $table->string('viz3_6')->nullable();
            $table->string('viz3_7')->nullable();
            $table->string('viz3_8')->nullable();
            $table->string('time_in_viz3')->nullable();
            $table->integer('likert_viz3_1')->nullable();
            $table->integer('likert_viz3_2')->nullable();
            $table->integer('likert_viz3_3')->nullable();
            $table->integer('likert_viz3_4')->nullable();
            $table->integer('likert_viz3_5')->nullable();
            $table->integer('likert_viz3_6')->nullable();
            $table->integer('likert_viz3_7')->nullable();
            $table->integer('likert_viz3_8')->nullable();
            $table->integer('likert_viz3_9')->nullable();
            $table->integer('likert_viz3_10')->nullable();
            $table->integer('likert_viz3_11')->nullable();
            $table->integer('likert_viz3_12')->nullable();
            $table->string('time_in_likert_viz3')->nullable();
            $table->string('viz4_1')->nullable();
            $table->string('viz4_2')->nullable();
            $table->string('viz4_3')->nullable();
            $table->string('viz4_4')->nullable();
            $table->string('viz4_5')->nullable();
            $table->string('viz4_6')->nullable();
            $table->string('viz4_7')->nullable();
            $table->string('viz4_8')->nullable();
            $table->string('time_in_viz4')->nullable();
            $table->integer('likert_viz4_1')->nullable();
            $table->integer('likert_viz4_2')->nullable();
            $table->integer('likert_viz4_3')->nullable();
            $table->integer('likert_viz4_4')->nullable();
            $table->integer('likert_viz4_5')->nullable();
            $table->integer('likert_viz4_6')->nullable();
            $table->integer('likert_viz4_7')->nullable();
            $table->integer('likert_viz4_8')->nullable();
            $table->integer('likert_viz4_9')->nullable();
            $table->integer('likert_viz4_10')->nullable();
            $table->integer('likert_viz4_11')->nullable();
            $table->integer('likert_viz4_12')->nullable();
            $table->string('time_in_likert_viz4')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('surveys');
        Schema::dropIfExists('migrations');
    }
}
