@section('progress-bar')
<div class="sticky-progress-bar">
  <div class="progress progress-bar-striped progress-bar-animated" style="height: 24px;">
    <div class="progress-bar" role="progressbar" style="width: {{ $progress }};" aria-valuenow="{{ $progress }}" aria-valuemin="0" aria-valuemax="100">{{ $progress }}</div>
  </div>
</div>
