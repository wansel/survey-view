<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Experimento</title>
  <link rel="stylesheet" href="/css/app.css">
  <link rel="stylesheet" href="{{ URL::asset('css/viewStyle.css') }}">
  <script type="text/javascript" src="{{ URL::asset('js/View.js') }}"></script>

</head>
  <body>

    <div id="viewAppContainer">
          <div id="splitView">
            @yield('viewShowcase')
          </div>

          <div id="splitSurvey">
            {{-- Display error messages --}}
            @include('inc.messages')
            @yield('content')
          </div>
    </div>
  <!-- <footer id="footer" class="text-center"> -->
    <!-- <p>Copyright 2018 &copy; <a href="http://www.nees.com">NEES - Núcleo de Excelência em Tecnologias Sociais</a></p> -->
    <br/>
    <br/>
  <!-- </footer> -->
    <script type="text/javascript" src="{{ URL::asset('js/View.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ URL::asset('js/barChart.js') }}"></script> -->
    @include('inc.progress-bar')
    @include('inc.bootstrap-material-design')
    <!-- <script type="text/javascript" src="{{ URL::asset('js/split.min.js') }}"></script> -->
    <!-- <script>
        var split = Split(['#splitView', '#splitContent'], {sizes:[60,40]});
    </script> -->
  </body>
</html>
