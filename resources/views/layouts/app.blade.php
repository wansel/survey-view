<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>Experimento</title>
  <link rel="stylesheet" href="/css/app.css">
  <link rel="stylesheet" href="{{ URL::asset('css/viewStyle.css') }}">
  <script type="text/javascript" src="{{ URL::asset('js/View.js') }}"></script>

</head>
  <body>
    <div class="container">
      @include('inc.showcase')
      <section>
        <div class="row">
          <div class="col-md-10 col-lg-10">
            {{-- Display error messages --}}
            @include('inc.messages')
            @yield('content')
          </div>
          <div class="col-md-2 col-lg-2">
            @if(!Request::is('/'))
              @include('inc.sidebar')
            @endif
          </div>
          </div>
      </section>
    </div>
  <footer id="footer" class="text-center">
    <p>Copyright 2018 &copy; <a href="http://www.nees.com">NEES - Núcleo de Excelência em Tecnologias Sociais</a></p>
    <br/>
    <br/>
  </footer>
    <script type="text/javascript" src="{{ URL::asset('js/View.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ URL::asset('js/barChart.js') }}"></script> -->
    @include('inc.progress-bar')
    @include('inc.bootstrap-material-design')
  </body>
</html>
