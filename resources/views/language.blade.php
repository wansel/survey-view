<?php $progress = "00%"; ?>
@extends('layouts.app')

@section('showcase')
    <h2>Avaliação da carga cognitiva enfrenta na tomada de decisões pedagógicas</h2>
    <h4>Título em inglês</h4>
    <h4>Título em espanhol</h4>
    <!-- <p class="lead">Por favor, escolha o idioma.</p>
    <p class="lead">Por favor, elige el idioma.</p>
    <p class="lead">Please, choose the language.</p> -->
@endsection

@section('content')
<div class="row mx-auto text-center">
<div class="card-deck text-center mx-auto">
  <div class="card" style="width: 18rem;">
    <img class="card-img-top img-responsive" src="/images/new_flag_br.png" alt="Portguês">
    <div class="card-body">
      <h5 class="card-title">Português</h5>
    </div>
    <div class="card-footer">
      <a href="/pt" class="btn btn-lg btn-primary ">Responder</a>
    </div>
  </div>
  <div class="card" style="width: 18rem;">
      <img class="card-img-top img-responsive" src="/images/new_flag_esp.png" alt="Español">
    <div class="card-body">
      <h5 class="card-title">Espanõl</h5>
    </div>
    <div class="card-footer">
      <a href="#" class="btn btn-lg btn-primary" disabled>Responder</a>
    </div>
  </div>
  <div href="/en">
    <div class="card" style="width: 18rem;">
      <img class="card-img-top img-responsive" src="/images/new_flag_usa.png" alt="English">
      <div class="card-body">
        <h5 class="card-title">English</h5>
      </div>
      <div class="card-footer">
        <a href="#" class="btn btn-lg btn-primary" disabled>Answer</a>
      </div>
    </div>
  </div>

</div>
</div>

    <br>
    <br>
@endsection
