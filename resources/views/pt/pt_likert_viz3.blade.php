<?php $progress = "72%"; ?>

@extends('layouts.app')

@section('showcase')
    <h2>Avaliação das Bolhas de Influência</h2>
@endsection

@section('content')
    {!! Form::open(['action' => 'SurveysController@storeLikertViz3', 'method' => 'POST']) !!}


            <div class="container form-group">
                <p class="lead">01. Os recursos apresentados me auxiliaram a tomar uma decisão de maneira eficiente.</p>
                <div class="form-row align-items-center center text-center">
                    <div class="col col-xs-12 survey-radio-box">
                        Discordo Completamente
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">1</div>
                        <label>{{ Form::radio('likert_viz3_1', '1') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">2</div>
                        <label>{{ Form::radio('likert_viz3_1', '2') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">3</div>
                        <label>{{ Form::radio('likert_viz3_1', '3') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">4</div>
                        <label>{{ Form::radio('likert_viz3_1', '4') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">5</div>
                        <label>{{ Form::radio('likert_viz3_1', '5') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">6</div>
                        <label>{{ Form::radio('likert_viz3_1', '6') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">7</div>
                        <label>{{ Form::radio('likert_viz3_1', '7') }}</label>
                    </div>
                    <div class="col col-xs-12 survey-radio-box">
                        Concordo Completamente
                    </div>
                </div>
            </div>


            <div class="container form-group">
                <p class="lead">02. Os recursos apresentados facilitaram a compreensão da situação de meus estudantes.</p>
                <div class="form-row align-items-center center text-center">
                    <div class="col col-xs-12 survey-radio-box">
                        Discordo Completamente
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">1</div>
                        <label>{{ Form::radio('likert_viz3_2', '1') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">2</div>
                        <label>{{ Form::radio('likert_viz3_2', '2') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">3</div>
                        <label>{{ Form::radio('likert_viz3_2', '3') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">4</div>
                        <label>{{ Form::radio('likert_viz3_2', '4') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">5</div>
                        <label>{{ Form::radio('likert_viz3_2', '5') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">6</div>
                        <label>{{ Form::radio('likert_viz3_2', '6') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">7</div>
                        <label>{{ Form::radio('likert_viz3_2', '7') }}</label>
                    </div>
                    <div class="col col-xs-12 survey-radio-box">
                        Concordo Completamente
                    </div>
                </div>
            </div>


            <div class="container form-group">
                <p class="lead">03. Os recursos apresentados foram claros e compreensíveis.</p>
                <div class="form-row align-items-center center text-center">
                    <div class="col col-xs-12 survey-radio-box">
                        Discordo Completamente
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">1</div>
                        <label>{{ Form::radio('likert_viz3_3', '1') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">2</div>
                        <label>{{ Form::radio('likert_viz3_3', '2') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">3</div>
                        <label>{{ Form::radio('likert_viz3_3', '3') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">4</div>
                        <label>{{ Form::radio('likert_viz3_3', '4') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">5</div>
                        <label>{{ Form::radio('likert_viz3_3', '5') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">6</div>
                        <label>{{ Form::radio('likert_viz3_3', '6') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">7</div>
                        <label>{{ Form::radio('likert_viz3_3', '7') }}</label>
                    </div>
                    <div class="col col-xs-12 survey-radio-box">
                        Concordo Completamente
                    </div>
                </div>
            </div>


            <div class="container form-group">
                <p class="lead">04. Foi fácil tomar uma decisão.</p>
                <div class="form-row align-items-center center text-center">
                    <div class="col col-xs-12 survey-radio-box">
                        Discordo Completamente
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">1</div>
                        <label>{{ Form::radio('likert_viz3_4', '1') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">2</div>
                        <label>{{ Form::radio('likert_viz3_4', '2') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">3</div>
                        <label>{{ Form::radio('likert_viz3_4', '3') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">4</div>
                        <label>{{ Form::radio('likert_viz3_4', '4') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">5</div>
                        <label>{{ Form::radio('likert_viz3_4', '5') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">6</div>
                        <label>{{ Form::radio('likert_viz3_4', '6') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">7</div>
                        <label>{{ Form::radio('likert_viz3_4', '7') }}</label>
                    </div>
                    <div class="col col-xs-12 survey-radio-box">
                        Concordo Completamente
                    </div>
                </div>
            </div>


            <div class="container form-group">
                <p class="lead">05. Os recursos apresentados possuíam bom design e estilo.</p>
                <div class="form-row align-items-center center text-center">
                    <div class="col col-xs-12 survey-radio-box">
                        Discordo Completamente
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">1</div>
                        <label>{{ Form::radio('likert_viz3_5', '1') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">2</div>
                        <label>{{ Form::radio('likert_viz3_5', '2') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">3</div>
                        <label>{{ Form::radio('likert_viz3_5', '3') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">4</div>
                        <label>{{ Form::radio('likert_viz3_5', '4') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">5</div>
                        <label>{{ Form::radio('likert_viz3_5', '5') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">6</div>
                        <label>{{ Form::radio('likert_viz3_5', '6') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">7</div>
                        <label>{{ Form::radio('likert_viz3_5', '7') }}</label>
                    </div>
                    <div class="col col-xs-12 survey-radio-box">
                        Concordo Completamente
                    </div>
                </div>
            </div>

            <div class="container form-group">
                <p class="lead">06. Os recursos apresentados são esteticamente atraentes.</p>
                <div class="form-row align-items-center center text-center">
                    <div class="col col-xs-12 survey-radio-box">
                        Discordo Completamente
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">1</div>
                        <label>{{ Form::radio('likert_viz3_6', '1') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">2</div>
                        <label>{{ Form::radio('likert_viz3_6', '2') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">3</div>
                        <label>{{ Form::radio('likert_viz3_6', '3') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">4</div>
                        <label>{{ Form::radio('likert_viz3_6', '4') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">5</div>
                        <label>{{ Form::radio('likert_viz3_6', '5') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">6</div>
                        <label>{{ Form::radio('likert_viz3_6', '6') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">7</div>
                        <label>{{ Form::radio('likert_viz3_6', '7') }}</label>
                    </div>
                    <div class="col col-xs-12 survey-radio-box">
                        Concordo Completamente
                    </div>
                </div>
            </div>

            <div class="container form-group">
                <p class="lead">07. Foi divertido realizar o que me foi proposto.</p>
                <div class="form-row align-items-center center text-center">
                    <div class="col col-xs-12 survey-radio-box">
                        Discordo Completamente
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">1</div>
                        <label>{{ Form::radio('likert_viz3_7', '1') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">2</div>
                        <label>{{ Form::radio('likert_viz3_7', '2') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">3</div>
                        <label>{{ Form::radio('likert_viz3_7', '3') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">4</div>
                        <label>{{ Form::radio('likert_viz3_7', '4') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">5</div>
                        <label>{{ Form::radio('likert_viz3_7', '5') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">6</div>
                        <label>{{ Form::radio('likert_viz3_7', '6') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">7</div>
                        <label>{{ Form::radio('likert_viz3_7', '7') }}</label>
                    </div>
                    <div class="col col-xs-12 survey-radio-box">
                        Concordo Completamente
                    </div>
                </div>
            </div>

            <div class="container form-group">
                <p class="lead">08. Gostaria de ter essa ferramenta no meu dia a dia como professor/tutor.</p>
                <div class="form-row align-items-center center text-center">
                    <div class="col col-xs-12 survey-radio-box">
                        Discordo Completamente
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">1</div>
                        <label>{{ Form::radio('likert_viz3_8', '1') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">2</div>
                        <label>{{ Form::radio('likert_viz3_8', '2') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">3</div>
                        <label>{{ Form::radio('likert_viz3_8', '3') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">4</div>
                        <label>{{ Form::radio('likert_viz3_8', '4') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">5</div>
                        <label>{{ Form::radio('likert_viz3_8', '5') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">6</div>
                        <label>{{ Form::radio('likert_viz3_8', '6') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">7</div>
                        <label>{{ Form::radio('likert_viz3_8', '7') }}</label>
                    </div>
                    <div class="col col-xs-12 survey-radio-box">
                        Concordo Completamente
                    </div>
                </div>
            </div>

            <div class="container form-group">
                <p class="lead">09. Eu utilizaria essa ferramenta se ela estivesse disponível.</p>
                <div class="form-row align-items-center center text-center">
                    <div class="col col-xs-12 survey-radio-box">
                        Discordo Completamente
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">1</div>
                        <label>{{ Form::radio('likert_viz3_9', '1') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">2</div>
                        <label>{{ Form::radio('likert_viz3_9', '2') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">3</div>
                        <label>{{ Form::radio('likert_viz3_9', '3') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">4</div>
                        <label>{{ Form::radio('likert_viz3_9', '4') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">5</div>
                        <label>{{ Form::radio('likert_viz3_9', '5') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">6</div>
                        <label>{{ Form::radio('likert_viz3_9', '6') }}</label>
                    </div>
                    <div class="col-sm col-xs-12 radio survey-radio-box">
                        <div class="survey-radio-label">7</div>
                        <label>{{ Form::radio('likert_viz3_9', '7') }}</label>
                    </div>
                    <div class="col col-xs-12 survey-radio-box">
                        Concordo Completamente
                    </div>
                </div>
            </div>


            <div class="form-group">
                <p class="lead">10. Descreva os principais aspectos POSITIVOS encontrados.</p>
                <!-- {{Form::label('likert_viz3_10', '10. Descreva os principais aspectos POSITIVOS encontrados.', ['class'=>'lead'])}} -->
                {{Form::textarea('likert_viz3_10', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>


            <div class="form-group">
                <p class="lead">11. Descreva os principais aspectos NEGATIVOS encontrados</p>
                {{Form::textarea('likert_viz3_11', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>


            <div class="form-group">
                <p class="lead">12. Por favor, inclua os demais comentários que achar necessário sobre o experimento.</p>
                {{Form::textarea('likert_viz3_12', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <br>
            <br>

    <a class="btn btn-secondary" href="{{ URL::previous() }}">Voltar</a>
    {{Form::submit('Avançar', ['class' => 'btn btn-raised btn-primary']) }}

    {!! Form::close() !!}
@endsection

@section('sidebar')
    1
    2
    3
@endsection
