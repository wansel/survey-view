<?php $progress = "45%"; ?>

@extends('layouts.viewApp')

@section('viewShowcase')
    <div id="viewContainer"></div>
@endsection

@section('content')

<p class="lead">
  Conjunto de Pesos Ordenados:<br/> As interações com o sistema são representadas como “pesos” proporcionais entre si. Quanto maior o peso, mais significativa a interação foi para o cálculo do nível de desempenho do aluno.
</p>

<p>Por favor, analise os pesos ordenados e responda as perguntas:</p>

    {!! Form::open(['action' => 'SurveysController@storeViz2', 'method' => 'POST']) !!}

            <!-- OW 01 -->
            <p class="lead">01. Qual o recurso <b>mais</b> influenciou positivamente os desempenhos dos alunos?</p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz2_1', 1, false)}} Questões Resolvidas</label><br/>
                    <label>{{Form::radio('viz2_1', 2, false)}} Questões Certas</label><br/>
                    <label>{{Form::radio('viz2_1', 3, false)}} Pontos</label><br/>
                    <label>{{Form::radio('viz2_1', 4, false)}} Sessões de Login</label><br/>
                    <label>{{Form::radio('viz2_1', 5, false)}} Vídeos Assistidos</label><br/>
                </div>
            </div>

            <!-- OW 02 -->
            <p class="lead">02. Qual recurso <b>menos</b> influenciou positivamente o desempenho dos alunos?</p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz2_2', 1, false)}} Questões Resolvidas</label><br/>
                    <label>{{Form::radio('viz2_2', 2, false)}} Questões Erradas</label><br/>
                    <label>{{Form::radio('viz2_2', 3, false)}} Pontos</label><br/>
                    <label>{{Form::radio('viz2_2', 4, false)}} Sessões de Login</label><br/>
                    <label>{{Form::radio('viz2_2', 5, false)}} Vídeos Assistidos</label><br/>
                </div>
            </div>
            <p class="lead">03. Qual recurso <b>não está</b> influenciando positivamente o desempenho dos alunos?</p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz2_3', 1, false)}} Questões Resolvidas</label><br/>
                    <label>{{Form::radio('viz2_3', 2, false)}} Questões Erradas</label><br/>
                    <label>{{Form::radio('viz2_3', 3, false)}} Questões Certas</label><br/>
                    <label>{{Form::radio('viz2_3', 4, false)}} Pontos</label><br/>
                    <label>{{Form::radio('viz2_3', 5, false)}} Sessões de Login</label><br/>
                </div>
            </div>
            <p class="lead">04. Quais recursos estão influenciando igualmente o desempenho dos alunos?</p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz2_4', 1, false)}} Questões Resolvidas e Questões Erradas</label><br/>
                    <label>{{Form::radio('viz2_4', 2, false)}} Questões Certas e Pontos</label><br/>
                    <label>{{Form::radio('viz2_4', 3, false)}} Pontos e Sessões de Login</label><br/>
                    <label>{{Form::radio('viz2_4', 4, false)}} Sessões de Login e Vídeos Assistidos</label><br/>
                    <label>{{Form::radio('viz2_4', 5, false)}} Vídeos Assistidos e Questões Certas</label><br/>
                </div>
            </div>
            <p class="lead">05. Qual conjunto de recursos é mais importante na classificação do aluno?</p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz2_5', 1, false)}} Resolver Questões e Questões Certas</label><br/>
                    <label>{{Form::radio('viz2_5', 2, false)}} Assistir Vídeos e Ganhar Pontos</label><br/>
                    <label>{{Form::radio('viz2_5', 3, false)}} Sessões de Login e Vídeos Assistidos</label><br/>
                    <label>{{Form::radio('viz2_5', 4, false)}} Questões Resolvidas e Sessões de Login</label><br/>
                    <label>{{Form::radio('viz2_5', 5, false)}} Pontos e Questões Certas</label><br/>
                </div>
            </div>


            <div class="form-group">
            <p class="lead">06. Quais atividades você recomendaria para o grupo de desempenho inadequado?</p>
                {{Form::textarea('viz2_6', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <div class="form-group">
            <p class="lead">07. Quais atividades você recomendaria para o grupo de desempenho insuficiente?</p>
                {{Form::textarea('viz2_7', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <div class="form-group">
            <p class="lead">08. Quais atividades você recomendaria para o grupo de desempenho adequado?</p>
                {{Form::textarea('viz2_8', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <br>
            <br>

        <a class="btn btn-secondary" href="{{ URL::previous() }}">Voltar</a>
        {{Form::submit('Avançar', ['class' => 'btn btn-raised btn-primary']) }}

    {!! Form::close() !!}

    <script type="text/javascript" src="{{ URL::asset('js/weights.js') }}"></script>
@endsection
