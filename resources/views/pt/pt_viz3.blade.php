<?php $progress = "63%"; ?>

@extends('layouts.viewApp')

@section('viewShowcase')
    <div id="viewContainer"></div>
@endsection

@section('content')

  <p class="lead">
  Bolhas de influência:<br/> Cada grupo de aprendizado é representado com os comportamentos chave que atraem (mantém) ou afastam o aluno do grupo.</p>

  <p>Por favor, analise as bolhas de influência e responda as perguntas:</p>

    {!! Form::open(['action' => 'SurveysController@storeViz3', 'REPLACEmethod' => 'POST']) !!}


            <p class="lead">01. Como "Acertar Questões" está influenciando os grupos de desempenho?</p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz3_1', 1, false)}} Atrai os alunos para o desempenho adequado e atrai os alunos para o desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_1', 2, false)}} Atrai os alunos para o desempenho adequado e afasta os alunos do desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_1', 3, false)}} Afasta os alunos para o desempenho adequado e afasta os alunos do desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_1', 4, false)}} Afasta os alunos para o desempenho adequado e atrai os alunos do desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_1', 5, false)}} Não está influenciando o desempenho dos alunos nos dois grupos.</label><br/>
                </div>
            </div>
            <p class="lead">02. Como "Ganhar Pontos" está influenciando os grupos de desempenho?</p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz3_2', 1, false)}} Atrai os alunos para o desempenho adequado e afasta os alunos do desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_2', 2, false)}} Atrai os alunos para o desempenho adequado e atrai os alunos do desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_2', 3, false)}} Afasta os alunos para o desempenho adequado e não influencia os alunos do desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_2', 4, false)}} Afasta os alunos para o desempenho adequado e atrai os alunos do desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_2', 5, false)}} Não está influenciando o desempenho dos alunos nos dois grupos.</label><br/>
                </div>
            </div>
            <p class="lead">03. Como “Assistir Vídeos” está influenciando os grupos de desempenho?</p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz3_3', 1, false)}} Atrai os alunos para o desempenho adequado e afasta os alunos do desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_3', 2, false)}} Atrai os alunos para o desempenho adequado e atrai os alunos para o desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_3', 3, false)}} Não influencia os alunos do desempenho adequado e atrai os alunos para o desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_3', 4, false)}} Afasta os alunos para o desempenho adequado e atrai os alunos do desempenho inadequado</label><br/>
                    <label>{{Form::radio('viz3_3', 5, false)}} Não está influenciando o desempenho dos alunos nos dois grupos.</label><br/>
                </div>
            </div>
            <p class="lead">04. Quais interações não fazem parte do conjunto de características dos alunos do <b>desempenho inadequado?</b></p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz3_4', 1, false)}} Responder Questões e Ganhar Pontos</label><br/>
                    <label>{{Form::radio('viz3_4', 2, false)}} Sessões de Login e Postagens em Fóruns</label><br/>
                    <label>{{Form::radio('viz3_4', 3, false)}} Assistir Vídeos e Acertar Questões</label><br/>
                    <label>{{Form::radio('viz3_4', 4, false)}} Questões Erradas e Ganhar Pontos</label><br/>
                    <label>{{Form::radio('viz3_4', 5, false)}} Ganhar Pontos e Postagens em Fóruns</label><br/>
                </div>
            </div>
            <p class="lead">05. Quais interações não fazem parte do conjunto de características dos alunos do <b>desempenho adequado?</b></p>
            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz3_5', 1, false)}} Questões Resolvidas e Questões Certas</label><br/>
                    <label>{{Form::radio('viz3_5', 2, false)}} Questões Erradas e Assistir Vídeos</label><br/>
                    <label>{{Form::radio('viz3_5', 3, false)}} Questões Certas e Ganhar Pontos</label><br/>
                    <label>{{Form::radio('viz3_5', 4, false)}} Responder Questões e Sessões de Login</label><br/>
                    <label>{{Form::radio('viz3_5', 5, false)}} Sessões de Login e Questões Erradas</label><br/>
                </div>
            </div>


            <div class="form-group">
            <p class="lead">06. Quais atividades você recomendaria para o grupo de desempenho inadequado?</p>
                {{Form::textarea('viz3_6', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <div class="form-group">
            <p class="lead">07. Quais atividades você recomendaria para o grupo de desempenho insuficiente?</p>
                {{Form::textarea('viz3_7', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <div class="form-group">
            <p class="lead">08. Quais atividades você recomendaria para o grupo de desempenho adequado?</p>
                {{Form::textarea('viz3_8', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <br>
            <br>


        <a class="btn btn-secondary" href="{{ URL::previous() }}">Voltar</a>
        {{Form::submit('Avançar', ['class' => 'btn btn-raised btn-primary']) }}

    {!! Form::close() !!}

    <script type="text/javascript" src="{{ URL::asset('js/bubbles.js') }}"></script>
@endsection
