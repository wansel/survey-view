<?php $progress = "27%"; ?>

@extends('layouts.viewApp')

@section('viewShowcase')
    <div id="viewContainer"></div>
@endsection

@section('content')

<p class="lead">
  Conjunto de Gráficos em Barras:<br/> Cada gráfico em barras representa os dados das interações dos estudantes com um recurso específico, os gráficos podem se dimensionar para mostrar ao usuário as interações de maior peso ou as mais significativas, colocando em evidência os dados que requerem mais atenção do usuário.</p>

<p>Por favor, analisando o gráfico, responda as perguntas:</p>

    {!! Form::open(['action' => 'SurveysController@storeViz1', 'method' => 'POST']) !!}

        <p class="lead">01. Qual recurso possui a <b>maior quantidade de interações</b> no grupo de desempenho <b>adequado</b>?</p>
        <div class="form-group">
            <div class="radio">
                <label>{{Form::radio('viz1_1', 1, false)}} Questões Resolvidas</label><br/>
                <label>{{Form::radio('viz1_1', 2, false)}} Questões Erradas</label><br/>
                <label>{{Form::radio('viz1_1', 3, false)}} Pontos</label><br/>
                <label>{{Form::radio('viz1_1', 4, false)}} Sessões de Login</label><br/>
                <label>{{Form::radio('viz1_1', 5, false)}} Postagens no fórum</label><br/>
            </div>
        </div>
        <p class="lead">02. Qual recurso possui a <b>menor quantidade de interações</b>  no grupo de desempenho <b>insuficiente?</b></p>
        <div class="form-group">
            <div class="radio">
                <label>{{Form::radio('viz1_2', 1, false)}} Questões Resolvidas</label><br/>
                <label>{{Form::radio('viz1_2', 2, false)}} Questões Erradas</label><br/>
                <label>{{Form::radio('viz1_2', 3, false)}} Pontos</label><br/>
                <label>{{Form::radio('viz1_2', 4, false)}} Sessões de Login</label><br/>
                <label>{{Form::radio('viz1_2', 5, false)}} Postagens no fórum</label><br/>
            </div>
        </div>
        <p class="lead">03. Qual recurso possui a <b>maior quantidade de interações</b> nos <b>três grupos de desempenho</b>?</p>
        <div class="form-group">
            <div class="radio">
                <label>{{Form::radio('viz1_3', 1, false)}} Questões Resolvidas</label><br/>
                <label>{{Form::radio('viz1_3', 2, false)}} Questões Erradas</label><br/>
                <label>{{Form::radio('viz1_3', 3, false)}} Questões Certas</label><br/>
                <label>{{Form::radio('viz1_3', 4, false)}} Pontos</label><br/>
                <label>{{Form::radio('viz1_3', 5, false)}} Vídeos Assistidos</label><br/>
            </div>
        </div>
        <p class="lead">04. Qual recurso possui a menor quantidade de interações  no grupo de desempenho inadequado?</p>
        <div class="form-group">
            <div class="radio">
                <label>{{Form::radio('viz1_4', 1, false)}} Questões Resolvidas</label><br/>
                <label>{{Form::radio('viz1_4', 2, false)}} Questões Erradas</label><br/>
                <label>{{Form::radio('viz1_4', 3, false)}} Vídeos Assistidos</label><br/>
                <label>{{Form::radio('viz1_4', 4, false)}} Pontos</label><br/>
                <label>{{Form::radio('viz1_4', 5, false)}} Sessões de Login</label><br/>
            </div>
        </div>

        <!-- BC05 -->
        <p class="lead">05. Qual recurso possui a <b>maior quantidade de interações</b> no grupo de <b>desempenho insuficente</b>?</p>
        <div class="form-group">
            <div class="radio">
                <label>{{Form::radio('viz1_5', 1, false)}} Questões Resolvidas</label><br/>
                <label>{{Form::radio('viz1_5', 2, false)}} Pontos</label><br/>
                <label>{{Form::radio('viz1_5', 3, false)}} Questões Erradas</label><br/>
                <label>{{Form::radio('viz1_5', 4, false)}} Vídeos Assistidos</label><br/>
                <label>{{Form::radio('viz1_5', 5, false)}} Sessões de Login</label><br/>
            </div>
        </div>


        <div class="form-group">
        <p class="lead">06. Quais atividades você recomendaria para o grupo de desempenho inadequado?</p>
            {{Form::textarea('viz1_6', null, ['rows' => '5', 'class'=>'form-control']) }}
        </div>
        <div class="form-group">
        <p class="lead">07. Quais atividades você recomendaria para o grupo de desempenho insuficiente?</p>
            {{Form::textarea('viz1_7', null, ['rows' => '5', 'class'=>'form-control']) }}
        </div>
        <div class="form-group">
        <p class="lead">08. Quais atividades você recomendaria para o grupo de desempenho adequado?</p>
            {{Form::textarea('viz1_8', null, ['rows' => '5', 'class'=>'form-control']) }}
        </div>
        <br>
        <br>

        <a class="btn btn-secondary" href="{{ URL::previous() }}">Voltar</a>
        {{Form::submit('Avançar', ['class' => 'btn btn-raised btn-primary']) }}

    {!! Form::close() !!}

      <script type="text/javascript" src="{{ URL::asset('js/barChart.js') }}"></script>

@endsection
