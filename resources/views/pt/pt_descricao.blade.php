<?php $progress = "18%"; ?>



@extends('layouts.app')

@section('showcase')
    <h2>Descrição do Experimento</h2>
@endsection


@section('content')
    <div>
        <h2>Objetivo</h2>
        <p>Este questionário tem por objetivo avaliar se os 4 recursos de visualização de dados apresentados ajudam os professores na tomada de decisão sobre como apoiar o desempenho de seus estudantes e turmas.</p>
    </div>

    <div>
        <h2>Dados</h2>
        <p>Os dados neste experimento representam as interações de uma turma fictícia de alunos em um ambiente online de ensino.
        Nesse ambiente online de ensino, os alunos podem:</p>
        <ul>
            <li>realizar login em uma conta individual;</li>
            <li>assistir vídeos;</li>
            <li>resolver questões;</li>
            <li>interagir com amigos por meio de fóruns;</li>
            <li>responder simulados no final de um assunto.</li>
        </ul>
    </div>

    <div>
        <h2>Níveis de Desempenho</h2>
        A maneira como os estudantes interagem com o ambiente online de ensino os classificam em 3 grupos de níveis de desempenho, são eles:
        <!-- Learning levels -->
        <div class="row">
            <div class="col-sm-3">
                <object class="card-img-top img-responsive" data="/images/focus_inadequate.svg" type="image/svg+xml"/></object>
            </div>
            <div class="col-sm-9">
                <h4>Desempenho Inadequado</h4>
                Os alunos no grupo inadequado (grupo vermelho) estão muito abaixo do esperado pelo professor/instrutor.
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <object class="card-img-top img-responsive" data="/images/focus_insufficient.svg" type="image/svg+xml"/></object>
            </div>
            <div class="col-sm-9">
                <h4>Desempenho Insuficiente</h4>
                Desempenho Insuficiente: Alunos que estão no nível de desempenho insuficiente (grupo amarelo) estão entre os dois níveis de aprendizado, apresentam características que podem os levar para o próximo nível, mas algumas de suas atitudes em relação ao sistema se assemelham ao grupo de desempenho inadequado.
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <object class="card-img-top img-responsive" data="/images/focus_adequate.svg" type="image/svg+xml"/></object>
            </div>
            <div class="col-sm-9">
                <h4>Desempenho Adequado</h4>
                Alunos que estão no nível de desempenho adequado (grupo verde) estão realizando suas atividades de acordo com o que está sendo determinado pelo instrutor/professor e estão obtendo bons resultados.
            </div>
        </div>

        <!-- <ul>
            <li>Desempenho Inadequado: Os alunos no grupo inadequado (grupo vermelho) estão muito abaixo do esperado pelo professor/instrutor.</li>
            <li>Desempenho Insuficiente: Alunos que estão no nível de desempenho insuficiente (grupo amarelo) estão entre os dois níveis de aprendizado, apresentam características que podem os levar para o próximo nível, mas algumas de suas atitudes em relação ao sistema se assemelham ao grupo de desempenho inadequado.</li>
            <li>Desempenho Adequado: Alunos que estão no nível de desempenho adequado (grupo verde) estão realizando suas atividades de acordo com o que está sendo determinado pelo instrutor/professor e estão obtendo bons resultados.</li>
        </ul> -->

        Vários fatores podem levar um estudante a ser classificado em um desses três grupos. Esse experimento propõe que você, professor, analise os dados da turma por meio de técnicas de visualização de dados e tome decisões a partir de suas conclusões.
    </div>
<!-- End Learning Levels -->
    <div>
        <h2>As Visualizações de Dados</h2>
        Os dados da turma foram organizados e representados em 4 visualizações de dados, entende-se por visualização de dados qualquer técnica de representação de informações por meio gráfico, foram elas:

        <ul>
            <li>Conjunto de Gráficos em Barras: Cada gráfico em barras representa os dados das interações dos estudantes com um recurso específico, os gráficos podem se dimensionar para mostrar ao usuário as interações de maior peso ou as mais significativas, colocando em evidência os dados que requerem mais atenção do usuário.</li>
            <li>Conjunto de Pesos Ordenados: As interações com o sistema são representadas como “pesos” proporcionais entre si. Quanto maior o peso, mais significativa a interação foi para o cálculo do nível de desempenho do aluno.</li>
            <li>Bolhas de influência: Cada grupo de aprendizado é representado com os comportamentos chave que atraem (mantém) ou afastam o aluno do grupo.</li>
            <li>Caminho Pedagógico: O modo de classificação é representado de maneira lúdica : Cada estudante começa com o desempenho indeterminado e o nível de interação com cada recurso do ambiente de ensino guia o aluno para um grupo. É possível entender como o conjunto de interações contribuiu para a classificação do aluno.</li>
        </ul>
    </div>

    {!! Form::open(['action' => 'SurveysController@storeDescription', 'method' => 'POST']) !!}

        <p><b></p>
        {{Form::label('description', "A Descrição do experimento está apropriada (compreensível)?", ['class' => 'lead'])}}
        <ul class="list-unstyled">
            <li>{{Form::select('description',
                        array(
                            0 => 'Selecione uma Opção',
                            1 => 'Discordo Completamente',
                            2 => 'Discordo',
                            3 => 'Discordo Levemente',
                            4 => 'Nem Discordo, Nem Concordo',
                            5 => 'Concordo Levemente',
                            6 => 'Concordo',
                            7 => 'Concordo Completamente'
                            ), 0, ['class' => 'form-control'])}}
            </li>
        </ul>

        <br>
        <br>

        <a class="btn btn-secondary" href="{{ URL::previous() }}">Voltar</a>
        {{Form::submit('Avançar', ['class' => 'btn btn-raised btn-primary']) }}

    {!! Form::close() !!}

@endsection
