<?php $progress = "00%"; ?>
@extends('layouts.app')


@section('showcase')
    <h2>TCLE - Termo de Consentimento Livre e Esclarecido</h2>
@endsection

@section('content')
    {!! Form::open(['action' => 'SurveysController@storeTcle', 'method' => 'POST']) !!}

        <div class="form-group">
            Os dados desta pesquisa serão mantidos em sigilo, bem como a identificação pessoal dos respondentes; os dados serão usados para análise apenas coletivamente. Ao final das 4 etapas do questionário as informações fornecidas serão registradas.


            Ao aceitar este Termo de Consentimento Livre e Esclarecido, você afirma que:
            <ul>
                <li>Entendo que sou livre para aceitar ou recusar e que eu posso interromper minha participação a qualquer momento.</li>
                <li>Eu concordo que os dados coletados para o estudo sejam usados para os propósitos acima descritos.</li>
                <li>Leu atentamente todas informações.</li>
            </ul>
        </div>

        <div class="form-group bmd-form-group">
            <div class="checkbox">
            <label class="text-dark">{{Form::checkbox('accept', 1)}} Confirmo que li e concordo, completamente, com as informações do TCLE e concordo em participar do experimento. </label>
            </div>
        </div>

        <a class="btn btn-secondary" href="{{ URL::previous() }}">Voltar</a>
        {{Form::submit('Avançar', ['class' => 'btn btn-raised btn-primary']) }}

    {!! Form::close() !!}
@endsection

@section('sidebar')
    <ul class="list-group">
        <li class="list-group-item">Seleção do Idioma</li>
        <li class="list-group-item active">Aceitar o TCLE</li>
        <li class="list-group-item">Dados do Participante</li>
        <li class="list-group-item">Descrição do Experimento</li>
        <li class="list-group-item">Visualização 1</li>
        <li class="list-group-item">Avaliação da Visualização 1</li>
        <li class="list-group-item">Visualização 2</li>
        <li class="list-group-item">Avaliação da Visualização 2</li>
        <li class="list-group-item">Visualização 3</li>
        <li class="list-group-item">Avaliação da Visualização 3</li>
        <li class="list-group-item">Visualização 4</li>
        <li class="list-group-item">Avaliação da Visualização 4</li>
        <li class="list-group-item">Fim</li>
    </ul>
@endsection
