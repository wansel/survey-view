<?php $progress = "81%"; ?>

@extends('layouts.viewApp')

@section('viewShowcase')
    <div id="viewContainer">
        <img style="width:98vw" src="/images/pedagogicalpath.gif"></img>
    </div>
@endsection

@section('content')

    <p class="lead">
      Caminho Pedagógico:<br/> O modo de classificação é representado de maneira lúdica : Cada estudante começa com o desempenho indeterminado e o nível de interação com cada recurso do ambiente de ensino guia o aluno para um grupo. É possível entender como o conjunto de interações contribuiu para a classificação do aluno.</p>
    <p>Por favor, analise o caminho pedagógico e responda as perguntas:</p>

    {!! Form::open(['action' => 'SurveysController@storeViz4', 'method' => 'POST']) !!}


            <p class="lead">1. Qual dos conjuntos de características levam o estudante para o Desempenho Adequado?</p>

            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz4_1', 1, false)}} Questões Respondidas (Alta) e Acessos ao Sistema (Baixo)</label><br/>
                    <label>{{Form::radio('viz4_1', 2, false)}} Questões Certas (Baixa) e Troféus (Alta)</label><br/>
                    <label>{{Form::radio('viz4_1', 3, false)}} Questões Erradas (Alta) e Troféus (Alta)</label><br/>
                    <label>{{Form::radio('viz4_1', 4, false)}} Questões Erradas (Alta) e Acessos ao Sistema (Alta)</label><br/>
                    <label>{{Form::radio('viz4_1', 5, false)}} Questões Erradas (Alta) e Questões Certas (Baixa)</label><br/>
                </div>
            </div>
            <p class="lead">02. Em qual grupo de desempenho um aluno com as seguintes características seria classificado?</p>

            <table class="table">
            	<thead>
            		<tr>
            			<th scope="col">Característica</th>
            			<th scope="col">Nível de desempenho</th>
            		</tr>
            	</thead>
            	<tbody>
                <tr><td> Nível </td><td> Alto </td></tr>
                <tr><td> Pontos </td><td> Alto </td></tr>
                <tr><td> Questões Certas </td><td> Alto </td></tr>
                <tr><td> Questões Erradas </td><td> Baixo </td></tr>
                <tr><td> Questões Respondidas </td><td> Alto </td></tr>
                <tr><td> Sessões de Login </td><td> Baixo </td></tr>
                <tr><td> Troféus </td><td> Alto </td></tr>
                <tr><td> Vídeos Assistidos </td><td> Alto </td></tr>
            	</tbody>
            </table>

            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz4_2', 1, false)}} Desempenho Adequado </label><br/>
                    <label>{{Form::radio('viz4_2', 2, false)}} Desempenho Insuficiente </label><br/>
                    <label>{{Form::radio('viz4_2', 3, false)}} Desempenho Inadequado </label><br/>
                    <label>{{Form::radio('viz4_2', 4, false)}} Desempenho Adequado ou Insuficiente </label><br/>
                    <label>{{Form::radio('viz4_2', 5, false)}} Desempenho Inadequado </label><br/>
                </div>
            </div>
            <p class="lead">03. Em qual grupo de desempenho um aluno com as seguintes características pode ser classificado?</p>

            <table class="table">
            	<thead>
            		<tr>
            			<th scope="col">Característica</th>
            			<th scope="col">Nível de desempenho</th>
            		</tr>
            	</thead>
            	<tbody>
                <tr><td> Nível </td><td> Baixo </td></tr>
                <tr><td> Pontos </td><td> Baixo </td></tr>
                <tr><td> Questões Certas </td><td> Alto </td></tr>
                <tr><td> Questões Erradas </td><td> Baixo </td></tr>
                <tr><td> Questões Respondidas </td><td> Alto </td></tr>
                <tr><td> Sessões de Login </td><td> Baixo </td></tr>
                <tr><td> Troféus </td><td> Baixo </td></tr>
                <tr><td> Vídeos Assistidos </td><td> Baixo </td></tr>
            	</tbody>
            </table>

            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz4_3', 1, false)}} Desempenho Adequado e Desempenho Inadequado</label><br/>
                    <label>{{Form::radio('viz4_3', 2, false)}} Desempmenho Inadequado e Desempenho Insuficiente</label><br/>
                    <label>{{Form::radio('viz4_3', 3, false)}} Desempenho Adequado e Desempenho Insuficiente</label><br/>
                    <label>{{Form::radio('viz4_3', 4, false)}} Somente desempenho adequado</label><br/>
                    <label>{{Form::radio('viz4_3', 5, false)}} Somente desempenho insuficiente</label><br/>
                </div>
            </div>
            <p class="lead">04. Em qual(is) grupos alunos que possuem as seguintes características podem ser classificados?</p>

            <table class="table">
            	<thead>
            		<tr>
            			<th scope="col">Característica</th>
            			<th scope="col">Nível de desempenho</th>
            		</tr>
            	</thead>
            	<tbody>
                <tr><td> Questões Certas </td><td> - </td></tr>
                <tr><td> Questões Erradas </td><td> Alto </td></tr>
                <tr><td> Acessos ao Sistema </td><td> Baixo </td></tr>
                <tr><td> Troféus </td><td> Baixo </td></tr>
            	</tbody>
            </table>

            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz4_4', 1, false)}} Desempenho Adequado ou Desempenho Insuficiente</label><br/>
                    <label>{{Form::radio('viz4_4', 2, false)}} Desempenho Insuficiente ou Desempenho Inadequado</label><br/>
                    <label>{{Form::radio('viz4_4', 3, false)}} Desempenho Inadequado ou Desempenho Adequado</label><br/>
                    <label>{{Form::radio('viz4_4', 4, false)}} Somente Desempenho Inadequado</label><br/>
                    <label>{{Form::radio('viz4_4', 5, false)}} Todos os grupos de desempenho</label><br/>
                </div>
            </div>
            <p class="lead">05. Em qual(is) grupos alunos que possuem as seguintes características podem ser classificados?</p>

            <table class="table">
            	<thead>
            		<tr>
            			<th scope="col">Característica</th>
            			<th scope="col">Nível de desempenho</th>
            		</tr>
            	</thead>
            	<tbody>
                <tr><td> Nível </td><td> Baixo </td></tr>
                <tr><td> Questões Certas </td><td> Alto </td></tr>
                <tr><td> Questões Erradas </td><td> Baixo </td></tr>
            	</tbody>
            </table>


            <div class="form-group">
                <div class="radio">
                    <label>{{Form::radio('viz4_5', 1, false)}} Somente grupo de Desempenho Inadequado</label><br/>
                    <label>{{Form::radio('viz4_5', 2, false)}} Somente grupo de Desempenho Insuficiente</label><br/>
                    <label>{{Form::radio('viz4_5', 3, false)}} Somente grupo de Desempenho Adequado</label><br/>
                    <label>{{Form::radio('viz4_5', 4, false)}} Grupo de Desempenho Adequado ou Insuficiente</label><br/>
                    <label>{{Form::radio('viz4_5', 5, false)}} Grupo de Desempenho Insuficiente ou Inadequado</label><br/>
                </div>
            </div>


            <div class="form-group">
            <p class="lead">06. Quais atividades você recomendaria para o grupo de desempenho inadequado?</p>
                {{Form::textarea('viz4_6', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <div class="form-group">
            <p class="lead">07. Quais atividades você recomendaria para o grupo de desempenho insuficiente?</p>
                {{Form::textarea('viz4_7', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <div class="form-group">
            <p class="lead">08. Quais atividades você recomendaria para o grupo de desempenho adequado?</p>
                {{Form::textarea('viz4_8', null, ['rows' => '5', 'class'=>'form-control']) }}
            </div>
            <br>
            <br>


        <a class="btn btn-secondary" href="{{ URL::previous() }}">Voltar</a>
        {{Form::submit('Avançar', ['class' => 'btn btn-raised btn-primary']) }}

    {!! Form::close() !!}
@endsection
