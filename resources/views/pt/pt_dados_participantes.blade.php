<?php $progress = "9%"; ?>

@extends('layouts.app')

@section('showcase')
    <h2>Dados Pessoais</h2>

    <p class="lead"> Suas informações serão anonimizadas, os dados aqui passados servirão para caráter estatístico.</p>
@endsection

@section('content')
    {!! Form::open(['action' => 'SurveysController@storePersonalData', 'method' => 'POST']) !!}

    <section>
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h3 class="lead">Informações Básicas</h3>

          <div class="form-group">
              {{Form::label('name', 'Nome Completo')}}
              {{Form::text('name', null, ['class' => 'form-control'])}}
          </div>

          <div class="form-group">
              {{Form::label('email', 'E-Mail')}}
              {{Form::text('email', null, ['class' => 'form-control'])}}
          </div>

          <div class="form-group">
            {{Form::label('gender', 'Sexo')}}
            <ul class="list-unstyled">
                <li>{{Form::select('gender',
                                array(
                                    0 => 'Selecione uma Opção',
                                    1 => 'Feminino',
                                    2 => 'Masculino',
                                    3 => 'Outro',
                                    ), 0, ['class' => 'form-control'])}}
                </li>
          </div>

          <div class="form-group">
              {{Form::label('birth_date', 'Data de Nascimento')}}
              {{Form::date('birth_date', null, ['class' => 'form-control'])}}
          </div>

          <div class="form-group">
              {{Form::label('country', 'País')}}
              {{Form::text('country', null, ['class' => 'form-control'])}}
          </div>

          <div class="form-group">
              {{Form::label('state', 'Estado')}}
              {{Form::text('state', null, ['class' => 'form-control'])}}
          </div>

          <div class="form-group">
              {{Form::label('city', 'Cidade')}}
              {{Form::text('city', null, ['class' => 'form-control'])}}
        </div>
      </div>
    </div>
  </section>

  <section>
    <div class="row">
      <div class="col-lg-8 mx-auto">
        <h3 class="lead">Informações sobre sua experiência como docente</h3>
        <div class="form-group">
            {{Form::label('education_level', 'Qual seu maior nível de escolaridade?')}}
            {{Form::text('education_level', null, ['class' => 'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('occupation', 'Profissão')}}
            {{Form::text('occupation', null, ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
          {{Form::label('education_institution', 'Que tipo de instituição realizou seus estudos?')}}
          {{Form::text('education_institution', null, ['class' => 'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('occupation_work_regime', 'Qual o regime de trabalho como instrutor?')}}
            {{Form::text('occupation_work_regime', null, ['class' => 'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('occupation_time', 'Há quanto tempo atua com ensino?')}}
            {{Form::text('occupation_time', null, ['class' => 'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('occupation_level', 'Leciona para qual nível?')}}
            {{Form::text('occupation_level', null, ['class' => 'form-control'])}}
        </div>

        <div class="form-group">
            {{Form::label('technology_use', 'Faz uso de tecnologia para apoiar a atividade de ensino?')}}
            {{Form::text('technology_use', null, ['class' => 'form-control'])}}
        </div>
      </div>
    </div>
  </section>

        <br>
        <br>

        <a class="btn btn-secondary" href="{{ URL::previous() }}">Voltar</a>
        {{Form::submit('Avançar', ['class' => 'btn btn-raised btn-primary']) }}
    {!! Form::close() !!}
@endsection

@section('sidebar')
    <ul class="list-group">
        <li class="list-group-item">Seleção do Idioma</li>
        <li class="list-group-item">Aceitar o TCLE</li>
        <li class="list-group-item active">Dados do Participante</li>
        <li class="list-group-item">Descrição do Experimento</li>
        <li class="list-group-item">Visualização 1</li>
        <li class="list-group-item">Avaliação da Visualização 1</li>
        <li class="list-group-item">Visualização 2</li>
        <li class="list-group-item">Avaliação da Visualização 2</li>
        <li class="list-group-item">Visualização 3</li>
        <li class="list-group-item">Avaliação da Visualização 3</li>
        <li class="list-group-item">Visualização 4</li>
        <li class="list-group-item">Avaliação da Visualização 4</li>
        <li class="list-group-item">Fim</li>
    </ul>
@endsection
