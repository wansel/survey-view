<?php

namespace App\Http\Controllers;

use App\Survey;
use Illuminate\Http\Request;
use DateTime;

class SurveysController extends Controller
{
    public function storeTcle(Request $request) {
        $this->validate($request,[
            'accept' => 'required'
        ]);

        # Submit TCLE acceptance
        $survey = new Survey();
        $survey->accept = $request->input('accept');
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Set success message
        $success = 'TCLE Aceito';

        # Redirect
        return redirect('pt/pt_dados_participantes')->with('survey', $survey)->with('success', $success);

    }

    public function storePersonalData(Request $request) {
        # Validate fields
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'occupation' => 'required',
            'education_level' => 'required',
            'education_institution' => 'required',
            'occupation_work_regime' => 'required',
            'occupation_time' => 'required',
            'occupation_level' => 'required',
            'technology_use' => 'required'
        ]);

        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->name = $request->input('name');
        $survey->email = $request->input('email');
        $survey->gender = $request->input('gender');
        $survey->birth_date = $request->input('birth_date');
        $survey->country = $request->input('country');
        $survey->state = $request->input('state');
        $survey->city = $request->input('city');
        $survey->occupation = $request->input('occupation');
        $survey->education_level = $request->input('education_level');
        $survey->education_institution = $request->input('education_institution');
        $survey->occupation_work_regime = $request->input('occupation_work_regime');
        $survey->occupation_time = $request->input('occupation_time');
        $survey->occupation_level = $request->input('occupation_level');
        $survey->technology_use = $request->input('technology_use');
        $survey->time_in_user_data = new DateTime();
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_descricao')->with('success', 'Dados Pessoais Preenchidos com Sucesso!');

    }

    public function storeDescription(Request $request) {
        # Validate fields
        $this->validate($request,
            [
                'description' => 'required'
            ],
            [
                'required' => 'O campo :attribute deve ser preenchido!'
        ]);

        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->description = $request->input('description');
        $survey->time_in_description = new DateTime();
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_viz1')->with('success', 'Descrição Sucesso!');
    }

    public function storeViz1(Request $request) {
        # Validate fields
        $this->validate($request,
            [
                'viz1_1' => 'required',
                'viz1_2' => 'required',
                'viz1_3' => 'required',
                'viz1_4' => 'required',
                'viz1_5' => 'required',
                'viz1_6' => 'required',
                'viz1_7' => 'required',
                'viz1_8' => 'required'
            ],
            [
                'required' => 'O campo :attribute deve ser preenchido!'
            ]);

        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->time_in_viz1 = new DateTime();
        $survey->viz1_1 = $request->input('viz1_1');
        $survey->viz1_2 = $request->input('viz1_2');
        $survey->viz1_3 = $request->input('viz1_3');
        $survey->viz1_4 = $request->input('viz1_4');
        $survey->viz1_5 = $request->input('viz1_5');
        $survey->viz1_6 = $request->input('viz1_6');
        $survey->viz1_7 = $request->input('viz1_7');
        $survey->viz1_8 = $request->input('viz1_8');
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_likert_viz1')->with('success', 'Você analisou os Gráficos Segmentados!');
    }

    public function storeLikertViz1(Request $request) {
        # Validate fields
        $this->validate($request,
            [
                'likert_viz1_1' => 'required',
                'likert_viz1_2' => 'required',
                'likert_viz1_3' => 'required',
                'likert_viz1_4' => 'required',
                'likert_viz1_5' => 'required',
                'likert_viz1_6' => 'required',
                'likert_viz1_7' => 'required',
                'likert_viz1_8' => 'required',
                'likert_viz1_9' => 'required',
                'likert_viz1_10' => 'required',
                'likert_viz1_11' => 'required',
                'likert_viz1_12' => 'required',
            ],
            [
                'required' => 'O campo :attribute deve ser preenchido!',
                'not_in' => 'É preciso escolher uma opção!'
            ]);

        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->time_in_likert_viz1 = new DateTime();
        $survey->likert_viz1_1 = $request->input('likert_viz1_1');
        $survey->likert_viz1_2 = $request->input('likert_viz1_2');
        $survey->likert_viz1_3 = $request->input('likert_viz1_3');
        $survey->likert_viz1_4 = $request->input('likert_viz1_4');
        $survey->likert_viz1_5 = $request->input('likert_viz1_5');
        $survey->likert_viz1_6 = $request->input('likert_viz1_6');
        $survey->likert_viz1_7 = $request->input('likert_viz1_7');
        $survey->likert_viz1_8 = $request->input('likert_viz1_8');
        $survey->likert_viz1_9 = $request->input('likert_viz1_9');
        $survey->likert_viz1_10 = $request->input('likert_viz1_10');
        $survey->likert_viz1_11 = $request->input('likert_viz1_11');
        $survey->likert_viz1_12 = $request->input('likert_viz1_12');
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_viz2')->with('success', 'Você avaliou os Gráficos Segmentados!');
    }

    public function storeViz2(Request $request) {
        # Validate fields
        $this->validate($request,
            [
                'viz2_1' => 'required',
                'viz2_2' => 'required',
                'viz2_3' => 'required',
                'viz2_4' => 'required',
                'viz2_5' => 'required',
                'viz2_6' => 'required',
                'viz2_7' => 'required',
                'viz2_8' => 'required'
            ],
            [
                'required' => 'O campo :attribute deve ser preenchido!'
            ]);


        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->time_in_viz2 = new DateTime();
        $survey->viz2_1 = $request->input('viz2_1');
        $survey->viz2_2 = $request->input('viz2_2');
        $survey->viz2_3 = $request->input('viz2_3');
        $survey->viz2_4 = $request->input('viz2_4');
        $survey->viz2_5 = $request->input('viz2_5');
        $survey->viz2_6 = $request->input('viz2_6');
        $survey->viz2_7 = $request->input('viz2_7');
        $survey->viz2_8 = $request->input('viz2_8');
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_likert_viz2')->with('success', 'Você analisou os Pesos Ordenados!');
    }

    public function storeLikertViz2(Request $request) {
        # Validate fields
        $this->validate($request,
            [
                'likert_viz2_1' => 'required',
                'likert_viz2_2' => 'required',
                'likert_viz2_3' => 'required',
                'likert_viz2_4' => 'required',
                'likert_viz2_5' => 'required',
                'likert_viz2_6' => 'required',
                'likert_viz2_7' => 'required',
                'likert_viz2_8' => 'required',
                'likert_viz2_9' => 'required',
                'likert_viz2_10' => 'required',
                'likert_viz2_11' => 'required',
                'likert_viz2_12' => 'required',
            ],
            [
                'required' => 'O campo :attribute deve ser preenchido!',
                'not_in' => 'É preciso escolher uma opção!'
            ]);

        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->time_in_likert_viz2 = new DateTime();
        $survey->likert_viz2_1 = $request->input('likert_viz2_1');
        $survey->likert_viz2_2 = $request->input('likert_viz2_2');
        $survey->likert_viz2_3 = $request->input('likert_viz2_3');
        $survey->likert_viz2_4 = $request->input('likert_viz2_4');
        $survey->likert_viz2_5 = $request->input('likert_viz2_5');
        $survey->likert_viz2_6 = $request->input('likert_viz2_6');
        $survey->likert_viz2_7 = $request->input('likert_viz2_7');
        $survey->likert_viz2_8 = $request->input('likert_viz2_8');
        $survey->likert_viz2_9 = $request->input('likert_viz2_9');
        $survey->likert_viz2_10 = $request->input('likert_viz2_10');
        $survey->likert_viz2_11 = $request->input('likert_viz2_11');
        $survey->likert_viz2_12 = $request->input('likert_viz2_12');
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_viz3')->with('success', 'Você avaliou os Pesos Ordenados!');
    }

    public function storeViz3(Request $request) {
        # Validate fields
        $this->validate($request,
            [
                'viz3_1' => 'required',
                'viz3_2' => 'required',
                'viz3_3' => 'required',
                'viz3_4' => 'required',
                'viz3_5' => 'required',
                'viz3_6' => 'required',
                'viz3_7' => 'required',
                'viz3_8' => 'required'
            ],
            [
                'required' => 'O campo :attribute deve ser preenchido!'
        ]);

        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->time_in_viz3 = new DateTime();
        $survey->viz3_1 = $request->input('viz3_1');
        $survey->viz3_2 = $request->input('viz3_2');
        $survey->viz3_3 = $request->input('viz3_3');
        $survey->viz3_4 = $request->input('viz3_4');
        $survey->viz3_5 = $request->input('viz3_5');
        $survey->viz3_6 = $request->input('viz3_6');
        $survey->viz3_7 = $request->input('viz3_7');
        $survey->viz3_8 = $request->input('viz3_8');
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_likert_viz3')->with('success', 'Você analisou as Bolhas de Influência!');
    }

    public function storeLikertViz3(Request $request) {
        # Validate fields
        $this->validate($request,
            [
                'likert_viz3_1' => 'required',
                'likert_viz3_2' => 'required',
                'likert_viz3_3' => 'required',
                'likert_viz3_4' => 'required',
                'likert_viz3_5' => 'required',
                'likert_viz3_6' => 'required',
                'likert_viz3_7' => 'required',
                'likert_viz3_8' => 'required',
                'likert_viz3_9' => 'required',
                'likert_viz3_10' => 'required',
                'likert_viz3_11' => 'required',
                'likert_viz3_12' => 'required',
            ],
            [
                'required' => 'O campo :attribute deve ser preenchido!',
                'not_in' => 'É preciso escolher uma opção!'
            ]);

        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->time_in_likert_viz3 = new DateTime();
        $survey->likert_viz3_1 = $request->input('likert_viz3_1');
        $survey->likert_viz3_2 = $request->input('likert_viz3_2');
        $survey->likert_viz3_3 = $request->input('likert_viz3_3');
        $survey->likert_viz3_4 = $request->input('likert_viz3_4');
        $survey->likert_viz3_5 = $request->input('likert_viz3_5');
        $survey->likert_viz3_6 = $request->input('likert_viz3_6');
        $survey->likert_viz3_7 = $request->input('likert_viz3_7');
        $survey->likert_viz3_8 = $request->input('likert_viz3_8');
        $survey->likert_viz3_9 = $request->input('likert_viz3_9');
        $survey->likert_viz3_10 = $request->input('likert_viz3_10');
        $survey->likert_viz3_11 = $request->input('likert_viz3_11');
        $survey->likert_viz3_12 = $request->input('likert_viz3_12');
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_viz4')->with('success', 'Você avaliou as Bolhas de Influência!');
    }

    public function storeViz4(Request $request) {
        # Validate fields
        $this->validate($request,
            [
                'viz4_1' => 'required',
                'viz4_2' => 'required',
                'viz4_3' => 'required',
                'viz4_4' => 'required',
                'viz4_5' => 'required',
                'viz4_6' => 'required',
                'viz4_7' => 'required',
                'viz4_8' => 'required'
            ],
            [
                'required' => 'O campo :attribute deve ser preenchido!'
            ]);

        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->time_in_viz4 = new DateTime();
        $survey->viz4_1 = $request->input('viz4_1');
        $survey->viz4_2 = $request->input('viz4_2');
        $survey->viz4_3 = $request->input('viz4_3');
        $survey->viz4_4 = $request->input('viz4_4');
        $survey->viz4_5 = $request->input('viz4_5');
        $survey->viz4_6 = $request->input('viz4_6');
        $survey->viz4_7 = $request->input('viz4_7');
        $survey->viz4_8 = $request->input('viz4_8');
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_likert_viz4')->with('success', 'Você analisou o Caminho Pedagógico!');
    }

    public function storeLikertViz4(Request $request) {
        # Validate fields
        $this->validate($request,
            [
                'likert_viz4_1' => 'required',
                'likert_viz4_2' => 'required',
                'likert_viz4_3' => 'required',
                'likert_viz4_4' => 'required',
                'likert_viz4_5' => 'required',
                'likert_viz4_6' => 'required',
                'likert_viz4_7' => 'required',
                'likert_viz4_8' => 'required',
                'likert_viz4_9' => 'required',
                'likert_viz4_10' => 'required',
                'likert_viz4_11' => 'required',
                'likert_viz4_12' => 'required',
            ],
            [
                'required' => 'O campo :attribute deve ser preenchido!',
                'not_in' => 'É preciso escolher uma opção!'
            ]);

        # Get ID from session
        $id = $request->session()->pull('id');

        # Update changes
        $survey = Survey::find($id);
        $survey->time_in_likert_viz4 = new DateTime();
        $survey->likert_viz4_1 = $request->input('likert_viz4_1');
        $survey->likert_viz4_2 = $request->input('likert_viz4_2');
        $survey->likert_viz4_3 = $request->input('likert_viz4_3');
        $survey->likert_viz4_4 = $request->input('likert_viz4_4');
        $survey->likert_viz4_5 = $request->input('likert_viz4_5');
        $survey->likert_viz4_6 = $request->input('likert_viz4_6');
        $survey->likert_viz4_7 = $request->input('likert_viz4_7');
        $survey->likert_viz4_8 = $request->input('likert_viz4_8');
        $survey->likert_viz4_9 = $request->input('likert_viz4_9');
        $survey->likert_viz4_10 = $request->input('likert_viz4_10');
        $survey->likert_viz4_11 = $request->input('likert_viz4_11');
        $survey->likert_viz4_12 = $request->input('likert_viz4_12');
        $survey->save();

        # Put ID into session
        $request->session()->put('id', $survey->id);

        # Redirect
        return redirect('pt/pt_agradecimentos')->with('success', 'Você completou o experimento!');
    }

}
