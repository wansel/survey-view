<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function getHome()
    {
        return view('language');
    }

    public function getTcle()
    {
        return view('pt.pt_tcle');
    }

    public function getPersonalData()
    {
        return view('pt.pt_dados_participantes');
    }

    public function getDescription()
    {
        return view('pt.pt_descricao');
    }

    public function getViz1()
    {
        return view('pt.pt_viz1');
    }

    public function getLikertViz1()
    {
        return view('pt.pt_likert_viz1');
    }

    public function getViz2()
    {
        return view('pt.pt_viz2');
    }

    public function getLikertViz2()
    {
        return view('pt.pt_likert_viz2');
    }

    public function getViz3()
    {
        return view('pt.pt_viz3');
    }

    public function getLikertViz3()
    {
        return view('pt.pt_likert_viz3');
    }

    public function getViz4()
    {
        return view('pt.pt_viz4');
    }

    public function getLikertViz4()
    {
        return view('pt.pt_likert_viz4');
    }

    public function getThanksPage()
    {
        return view('pt.pt_agradecimentos');
    }

    public function getVersion()
    {
        $pageNumber = rand(1, 3);

        if($pageNumber == 1) {
            return view('v1.pt_v1_p1');

        } elseif($pageNumber == 2) {
            return view('v2.pt_v2_p2');

        } else {
            return view('v3.pt_v3_p3');

        }

    }

}
