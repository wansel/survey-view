-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 14, 2018 at 12:22 AM
-- Server version: 5.6.38
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `survey`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_03_26_182813_surveys', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE `surveys` (
  `id` int(10) UNSIGNED NOT NULL,
  `accept` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education_level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education_institution` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation_work_regime` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `occupation_level` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `technology_use` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_user_data` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz1_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz1_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz1_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz1_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz1_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz1_6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz1_7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz1_8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_viz1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz1_1` int(11) DEFAULT NULL,
  `likert_viz1_2` int(11) DEFAULT NULL,
  `likert_viz1_3` int(11) DEFAULT NULL,
  `likert_viz1_4` int(11) DEFAULT NULL,
  `likert_viz1_5` int(11) DEFAULT NULL,
  `likert_viz1_6` int(11) DEFAULT NULL,
  `likert_viz1_7` int(11) DEFAULT NULL,
  `likert_viz1_8` int(11) DEFAULT NULL,
  `likert_viz1_9` int(11) DEFAULT NULL,
  `likert_viz1_10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz1_11` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz1_12` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_likert_viz1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz2_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz2_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz2_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz2_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz2_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz2_6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz2_7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz2_8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_viz2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz2_1` int(11) DEFAULT NULL,
  `likert_viz2_2` int(11) DEFAULT NULL,
  `likert_viz2_3` int(11) DEFAULT NULL,
  `likert_viz2_4` int(11) DEFAULT NULL,
  `likert_viz2_5` int(11) DEFAULT NULL,
  `likert_viz2_6` int(11) DEFAULT NULL,
  `likert_viz2_7` int(11) DEFAULT NULL,
  `likert_viz2_8` int(11) DEFAULT NULL,
  `likert_viz2_9` int(11) DEFAULT NULL,
  `likert_viz2_10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz2_11` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz2_12` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_likert_viz2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz3_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz3_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz3_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz3_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz3_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz3_6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz3_7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz3_8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_viz3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz3_1` int(11) DEFAULT NULL,
  `likert_viz3_2` int(11) DEFAULT NULL,
  `likert_viz3_3` int(11) DEFAULT NULL,
  `likert_viz3_4` int(11) DEFAULT NULL,
  `likert_viz3_5` int(11) DEFAULT NULL,
  `likert_viz3_6` int(11) DEFAULT NULL,
  `likert_viz3_7` int(11) DEFAULT NULL,
  `likert_viz3_8` int(11) DEFAULT NULL,
  `likert_viz3_9` int(11) DEFAULT NULL,
  `likert_viz3_10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz3_11` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz3_12` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_likert_viz3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz4_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz4_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz4_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz4_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz4_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz4_6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz4_7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `viz4_8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_viz4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz4_1` int(11) DEFAULT NULL,
  `likert_viz4_2` int(11) DEFAULT NULL,
  `likert_viz4_3` int(11) DEFAULT NULL,
  `likert_viz4_4` int(11) DEFAULT NULL,
  `likert_viz4_5` int(11) DEFAULT NULL,
  `likert_viz4_6` int(11) DEFAULT NULL,
  `likert_viz4_7` int(11) DEFAULT NULL,
  `likert_viz4_8` int(11) DEFAULT NULL,
  `likert_viz4_9` int(11) DEFAULT NULL,
  `likert_viz4_10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz4_11` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `likert_viz4_12` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_in_likert_viz4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `accept`, `name`, `email`, `gender`, `birth_date`, `country`, `state`, `city`, `education_level`, `occupation`, `education_institution`, `occupation_work_regime`, `occupation_time`, `occupation_level`, `technology_use`, `time_in_user_data`, `description`, `time_in_description`, `viz1_1`, `viz1_2`, `viz1_3`, `viz1_4`, `viz1_5`, `viz1_6`, `viz1_7`, `viz1_8`, `time_in_viz1`, `likert_viz1_1`, `likert_viz1_2`, `likert_viz1_3`, `likert_viz1_4`, `likert_viz1_5`, `likert_viz1_6`, `likert_viz1_7`, `likert_viz1_8`, `likert_viz1_9`, `likert_viz1_10`, `likert_viz1_11`, `likert_viz1_12`, `time_in_likert_viz1`, `viz2_1`, `viz2_2`, `viz2_3`, `viz2_4`, `viz2_5`, `viz2_6`, `viz2_7`, `viz2_8`, `time_in_viz2`, `likert_viz2_1`, `likert_viz2_2`, `likert_viz2_3`, `likert_viz2_4`, `likert_viz2_5`, `likert_viz2_6`, `likert_viz2_7`, `likert_viz2_8`, `likert_viz2_9`, `likert_viz2_10`, `likert_viz2_11`, `likert_viz2_12`, `time_in_likert_viz2`, `viz3_1`, `viz3_2`, `viz3_3`, `viz3_4`, `viz3_5`, `viz3_6`, `viz3_7`, `viz3_8`, `time_in_viz3`, `likert_viz3_1`, `likert_viz3_2`, `likert_viz3_3`, `likert_viz3_4`, `likert_viz3_5`, `likert_viz3_6`, `likert_viz3_7`, `likert_viz3_8`, `likert_viz3_9`, `likert_viz3_10`, `likert_viz3_11`, `likert_viz3_12`, `time_in_likert_viz3`, `viz4_1`, `viz4_2`, `viz4_3`, `viz4_4`, `viz4_5`, `viz4_6`, `viz4_7`, `viz4_8`, `time_in_viz4`, `likert_viz4_1`, `likert_viz4_2`, `likert_viz4_3`, `likert_viz4_4`, `likert_viz4_5`, `likert_viz4_6`, `likert_viz4_7`, `likert_viz4_8`, `likert_viz4_9`, `likert_viz4_10`, `likert_viz4_11`, `likert_viz4_12`, `time_in_likert_viz4`, `created_at`, `updated_at`) VALUES
(8, '1', 'Wansel', 'email', '2', '31/07/1996', 'pais', 'estado', 'cidade', 'qual seu maior nivel de escolaridade', 'profissao', 'que tipo de instituicao realizaou seus estudos', 'qual o regime de trabalho como instrutor', 'ha quanto tempo atua com ensino', 'leciona para qual nivel', 'faz uso de tecnologia para apoiar a ativdade de ensino', '2018-05-15 12:35:17', '6', '2018-05-15 12:35:22', '1', '2', '3', '4', '5', 'Quais atividades você recomendaria para o grupo de desempenho inadequado', 'Quais ativdidades você recomendaria para o grupo de desempenho insuficiente', 'quais atividade você recomendaria para o grupo de desempenho adequado', '2018-05-15 12:36:21', 1, 2, 3, 4, 5, 6, 7, 1, 2, 'Descreva os principais aspectos positivos encontrados', 'descreva os principais aspectos negativos encontrados', 'por favor, inclua os demais comentarios que achar necessario sobre o experimento', '2018-05-15 12:37:48', '1', '2', '3', '4', '5', 'Quais atividades você recomendaria para o grupo de desempenho inadequado?', 'Quais atividades você recomendaria para o grupo de desempenho insuficiente?', 'Quais atividades você recomendaria para o grupo de desempenho adequado?', '2018-05-15 12:38:21', 7, 6, 5, 4, 3, 2, 1, 2, 3, 'Descreva os principais aspectos POSITIVOS encontrados.', 'Descreva os principais aspectos NEGATIVOS encontrados', 'Por favor, inclua os demais comentários que achar necessário sobre o experimento.', '2018-05-15 12:39:09', '1', '2', '3', '4', '5', 'Quais atividades você recomendaria para o grupo de desempenho inadequado?', 'Quais atividades você recomendaria para o grupo de desempenho insuficiente?', 'Quais atividades você recomendaria para o grupo de desempenho adequado?', '2018-05-15 12:39:35', 1, 2, 3, 4, 5, 6, 7, 6, 5, 'Descreva os principais aspectos POSITIVOS encontrados.', 'Descreva os principais aspectos NEGATIVOS encontrados', 'Por favor, inclua os demais comentários que achar necessário sobre o experimento.', '2018-05-15 12:41:16', '2', '2', '3', '4', '5', 'Quais atividades você recomendaria para o grupo de desempenho inadequado?', 'Quais atividades você recomendaria para o grupo de desempenho insuficiente?', 'Quais atividades você recomendaria para o grupo de desempenho adequado?', '2018-05-15 12:41:36', 1, 2, 3, 4, 5, 6, 7, 6, 5, 'Descreva os principais aspectos POSITIVOS encontrados.', 'Descreva os principais aspectos NEGATIVOS encontrados', 'Por favor, inclua os demais comentários que achar necessário sobre o experimento.', '2018-05-15 12:41:57', '2018-05-15 15:34:00', '2018-05-15 15:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `surveys`
--
ALTER TABLE `surveys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `surveys`
--
ALTER TABLE `surveys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
